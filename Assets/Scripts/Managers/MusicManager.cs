using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioClip[] musicClip;
    private AudioSource musicSource;
    void Start()
    {
        musicSource = GetComponent<AudioSource>();

             musicSource.clip = musicClip[Random.Range(0, musicClip.Length)];
             musicSource.Play();
           
    }
    void Update()
    {
        if (!musicSource.isPlaying)
        {
            musicSource.clip = musicClip[Random.Range(0, musicClip.Length)];
            musicSource.Play();
        }
    } 
    
}
