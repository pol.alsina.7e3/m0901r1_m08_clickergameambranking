using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    AudioSource AudioSource;
    public List<AudioClip> sfxPenLightButton;
    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        AudioSource = GetComponent<AudioSource>();
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }
    public AudioSource GetAudioSource()
    {
        return AudioSource;
    }
    public void PlayClipButtonPenLight()
    {
        int rnd = Random.Range(0, sfxPenLightButton.Count);
        AudioSource.PlayOneShot(sfxPenLightButton[rnd]);
    }

    
}
