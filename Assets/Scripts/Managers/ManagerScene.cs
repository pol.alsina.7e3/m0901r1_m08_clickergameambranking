using UnityEngine.SceneManagement;
using UnityEngine;

public class ManagerScene : MonoBehaviour
{
    private static ManagerScene instance;
    public static ManagerScene Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void Play()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void Menu()
    {
        SceneManager.LoadScene("LoginScene");
    }

    public void Ranking()
    {
        SceneManager.LoadScene("RankingScene");
    }

}
