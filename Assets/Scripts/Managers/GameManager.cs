using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public string username;
    public string password;
    public int score;

    private static GameManager instance;
    public delegate void AnimUpSpeed();
    public static event AnimUpSpeed animUpSpeed;
    public static GameManager Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    public void ButtonClick()
    {
        score++;
        if (score % 10 == 0)
        {
            animUpSpeed();
        }
    }
}
