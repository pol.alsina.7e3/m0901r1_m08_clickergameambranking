using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UILogin : MonoBehaviour
{
    //private GameObject inputs;
    [SerializeField] private GameObject username;
    [SerializeField] private GameObject password;
    private string txtName;
    private string txtPassword;
    [SerializeField]  GameObject loading;

    public DataJson data;

    private void Awake()
    {
        
    }


    public void ButtonLogin()
    {
        GiveData();
        StartCoroutine(data.SendDataPlayer("/LogIn"));
        ActivcateLoadingScreen();

            
    }
    public void ButtonRegister()
    {
        GiveData();
        StartCoroutine(data.SendDataPlayer("/SignInUser"));
        ActivcateLoadingScreen();
    }

    public void ButtonRanking()
    {
        ManagerScene.Instance.Ranking();
    }

    public void GiveData()
    {
        GameManager.Instance.username = username.GetComponent<TMP_InputField>().text;
        GameManager.Instance.password = password.GetComponent<TMP_InputField>().text;
        GameManager.Instance.score = 0;
    }
    void ActivcateLoadingScreen()
    {
       loading.SetActive(true);
        loading.GetComponent<LodaingScreen>().StartLoading();
    }
}
