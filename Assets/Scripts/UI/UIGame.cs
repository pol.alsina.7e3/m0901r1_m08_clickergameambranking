using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGame : MonoBehaviour
{
    public DataJson data;

    public void Home()
    {
        StartCoroutine(data.UpdateScorePlayer("/UpdateScore"));
        ManagerScene.Instance.Menu();
    }
}
