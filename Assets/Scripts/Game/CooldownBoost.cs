using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CooldownBoost : MonoBehaviour
{
    public float seg;
    public TextMeshProUGUI cooldown;

    private void Start()
    {
        cooldown = GetComponent<TextMeshProUGUI>();
    }

    void Update()
    {
            seg -= Time.deltaTime;
            int tempSeg = Mathf.FloorToInt(seg % 60);
            cooldown.text = string.Format("Cooldown: {00}", tempSeg);
    }
    public void Destroy()
    {
        Destroy(this);
    }
}
