using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class BoostButtons : MonoBehaviour
{
    public VideoPlayer vP;
    public VideoClip peachesVideo;
    public VideoClip nicoNicoNiiVideo;
    bool canActivateBoost = true;
    GameObject cooldown;
    public float timeCooldown;

    public Button peaches;
    public Button nicoNi;

    private void Start()
    {
        vP.SetTargetAudioSource(0,AudioManager.Instance.GetAudioSource());
        cooldown = GameObject.Find("Cooldown");
    }


    public void BoostBurst()
    {
        if(canActivateBoost)
        {
            vP.clip = peachesVideo;
            vP.Play();
            Points();
            
        }
        
    }
    public void BoostNico()
    {
        if (canActivateBoost)
        {
            vP.clip = nicoNicoNiiVideo;
            vP.Play();
            Points();
        }
        
    }
    void Points()
    {
        int rnd = Random.Range(33, 333);
        GameManager.Instance.score += rnd;
        StartCoroutine(Cooldown());
    }
    IEnumerator Cooldown()
    {
        DesactivateOrActivateButtons(false);
        canActivateBoost = false;
        cooldown.AddComponent<CooldownBoost>().seg = timeCooldown;
        yield return new WaitForSeconds(timeCooldown);
        cooldown.GetComponent<CooldownBoost>().Destroy();
        cooldown.GetComponent<TextMeshProUGUI>().text = "";
        DesactivateOrActivateButtons(true);
        canActivateBoost = true;
    }
    void DesactivateOrActivateButtons(bool activated)
    {

        nicoNi.interactable = activated;
        peaches.interactable = activated;
    }
}
