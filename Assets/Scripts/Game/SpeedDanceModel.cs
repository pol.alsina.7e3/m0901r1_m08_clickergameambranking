using UnityEngine;

public class SpeedDanceModel : MonoBehaviour
{
    private Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void AugmentSpeedAnim()
    {
        animator.speed += 0.5f;
    }
    private void OnEnable()
    {
        GameManager.animUpSpeed += AugmentSpeedAnim;
    }
    private void OnDisable()
    {
        GameManager.animUpSpeed -= AugmentSpeedAnim;

    }
}
