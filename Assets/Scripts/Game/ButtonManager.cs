using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    Button clickerLight;   
    void Start()
    {
        clickerLight = GetComponent<Button>();
        clickerLight.onClick.AddListener(ClickLight);
    }

    void ClickLight()
    {
        GameManager.Instance.ButtonClick();
        AudioManager.Instance.PlayClipButtonPenLight();
    }
}
