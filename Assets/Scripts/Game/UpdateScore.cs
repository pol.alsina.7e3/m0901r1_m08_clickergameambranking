using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UpdateScore : MonoBehaviour
{
    TextMeshProUGUI textScore;
    private void Start()
    {
        textScore = GetComponent<TextMeshProUGUI>();
    }
    private void Update()
    {
        UpdateScoreText();
    }
    void UpdateScoreText()
    {
        textScore.text = GameManager.Instance.score.ToString();
    }
   
}
