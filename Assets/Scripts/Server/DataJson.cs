using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Networking;

public class DataJson : MonoBehaviour
{
    private string url = "http://localhost:50000";
    // Start is called before the first frame update
    void Start()
    {
       /* HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        Stream stream = response.GetResponseStream();
        StreamReader reader = new StreamReader(stream);
        string content = reader.ReadToEnd();
        Debug.Log(content);
        response.Close();*/
    }

    public IEnumerator SendDataPlayer(string method)
    {
        UserData uD = new UserData(GameManager.Instance.username, GameManager.Instance.password);
        Debug.Log(uD.password );
      
        string user = JsonConvert.SerializeObject(uD);

        var request = UnityWebRequest.Post(url + method, user);
        byte[] bodyRaw = Encoding.UTF8.GetBytes(user);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");


        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(request.error);
        }
        else
        {
            Debug.Log(request.downloadHandler.text);
            ManagerScene.Instance.Play();
        }
    }

    public IEnumerator UpdateScorePlayer(string method)
    {
        UserData uD = new UserData(GameManager.Instance.username, GameManager.Instance.score);
        string user = JsonConvert.SerializeObject(uD);

        var request = UnityWebRequest.Put(url + method, user);
        byte[] bodyRaw = Encoding.UTF8.GetBytes(user);
        request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");


        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(request.error);
        }
        else
        {
            Debug.Log(request.downloadHandler.text);
        }
    }
}

public class UserData
{
    public string username { get; set; }
    public string password { get; set; }
    public int score { get; set; }
    public UserData(string u, string p, int s)
    {
        username = u;
        password = p;   
        score = s;
    }

    public UserData(string u, string p)
    {
        username = u;
        password = p;
        score = 0;
    }

    public UserData(string u, int s)
    {
        username = u;
        password = null;
        score = s;
    }
}
